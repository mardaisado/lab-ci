#include "sum.h"

#include <stdint.h>

int64_t integer_avg(int a, int b)
{

    int64_t tempA = a;
    int64_t tempB = b;
    int64_t result = 0;

    result = (tempA + tempB)/2;

    return result;
}
