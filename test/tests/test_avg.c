#include "average.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg 1");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 1), "Error in integer_avg 2");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(1, 0), "Error in integer_avg 3");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(-1, 1), "Error in integer_avg 4");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(-1, 0), "Error in integer_avg 5");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_avg(-1, -1), "Error in integer_avg 6");
    TEST_ASSERT_EQUAL_INT_MESSAGE(12, integer_avg(10, 15), "Error in integer_avg 7");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-25, integer_avg(100, -150), "Error in integer_avg 8");

    // add test
    TEST_ASSERT_EQUAL_INT64_MESSAGE(INT32_MAX, integer_avg(INT32_MAX, INT32_MAX), "Error in integer_avg INT32_MAX+INT32_MAX");
    TEST_ASSERT_EQUAL_INT64_MESSAGE(INT32_MIN, integer_avg(INT32_MIN, INT32_MIN), "Error in integer_avg INT32_MIN+INT32_MIN");

    TEST_ASSERT_EQUAL_INT64_MESSAGE(0, integer_avg(INT32_MAX, INT32_MIN), "Error in integer_avg INT32_MAX+INT32_MIN");
   }

